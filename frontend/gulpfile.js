const { src, dest, parallel, series, watch } = require('gulp');

// Load plugins

const rename = require('gulp-rename');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const changed = require('gulp-changed');

function css() {
  const source = './src/styles/scss/*.scss';
  return src(source)
    .pipe(changed(source))
    .pipe(sass())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false,
      })
    )
    .pipe(
      rename({
        extname: '.min.css',
      })
    )
    .pipe(cssnano())
    .pipe(dest('./src/dist'));
}

// Watch files

function watchFiles() {
  watch('./src/styles/scss/*', css);
}

// Tasks to define the execution of the functions simultaneously or in series

exports.watch = parallel(watchFiles);
exports.default = series(parallel(css));
exports.build = series(parallel(css));
