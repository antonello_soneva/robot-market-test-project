/**
 *
 * @param { array } array
 * @returns array
 */
export const uniqueArray = (array = []) => {
  const cleanArray = [];
  array.forEach((element) => {
    if (!cleanArray.includes(element)) {
      cleanArray.push(element);
    }
  });
  return cleanArray;
};
