import moment from 'moment';
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'THB',
});
/**
 *
 * @param { number } amount
 * @returns string
 */
export const moneyFormatter = (amount) =>
  formatter.format(amount).replace('THB', '฿');
/**
 *
 * @param { string } date
 * @returns string
 */
export const dateFormatter = (date) => moment(date).format('DD/MM/YYYY');
