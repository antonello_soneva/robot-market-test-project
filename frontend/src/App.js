import * as React from 'react';
import Main from './components/main';
import { Provider } from 'react-redux';
import store from './redux/store';
import { ConfirmProvider } from 'material-ui-confirm';
function App() {
  return (
    <Provider store={store}>
      <ConfirmProvider>
        <div className="container App">
          <Main />
        </div>
      </ConfirmProvider>
    </Provider>
  );
}

export default App;
