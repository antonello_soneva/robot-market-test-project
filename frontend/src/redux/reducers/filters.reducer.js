import { produce } from 'immer';
import { SET_FILTERS } from '../types/filters.types';
import { filtersInitialState } from '../initialState';
/**
 * @param { object } state
 * @param { object } action
 * @returns { object } state
 */
export default (state = filtersInitialState, action) => {
  switch (action.type) {
    case SET_FILTERS:
      return produce(state, (draftState) => {
        const filters = action.payload;
        return { ...draftState, ...filters };
      });
    default:
      return state;
  }
};
