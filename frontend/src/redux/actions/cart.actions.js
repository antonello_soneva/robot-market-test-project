import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  UPDATE_CART,
} from '../types/cart.types';
/**
 *
 * @param { object } robot
 * @returns { object }
 */
export const addToCart = (robot) => {
  return {
    type: ADD_TO_CART,
    payload: robot,
  };
};
/**
 *
 * @param { string } robotName
 * @returns { object }
 */
export const removeFromCart = (robotName) => {
  return {
    type: REMOVE_FROM_CART,
    payload: robotName,
  };
};
/**
 *
 * @param { string } robotName
 * @param { string } operation
 * @param { number } quantity
 * @returns { object }
 */
export const updateCart = (robotName, operation, quantity) => {
  return {
    type: UPDATE_CART,
    payload: {
      robotName,
      operation,
      quantity,
    },
  };
};
