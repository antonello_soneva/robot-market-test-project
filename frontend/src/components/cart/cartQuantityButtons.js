import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import { AddCircleOutline, RemoveCircleOutline } from '@material-ui/icons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateCart } from '../../redux/actions/cart.actions';
import { setNotification } from '../../redux/actions/notification.actions';

const useStyles = makeStyles((theme) => ({
  root: {
    '& label.Mui-focused': {
      color: 'white',
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: '#ddd',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'white',
      },
      '&:hover fieldset': {
        borderColor: 'white',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#ddd',
      },
    },
  },
}));

const CartQuantityButtons = ({
  quantity,
  name,
  cart,
  updateCart,
  setNotification,
  handleRemoveFromCart,
}) => {
  const editRobotCart = (operation, value) => {
    const cartItemIndex = cart.findIndex((item) => item.name === name);
    if (cartItemIndex < 0) return;
    switch (operation) {
      case '+':
        if (cart[cartItemIndex].stock > cart[cartItemIndex].quantity) {
          updateCart(name, operation);
        } else {
          setNotification({
            message: 'Not Enough in stock',
            severity: 'error',
          });
        }
        break;
      case '-':
        if (cart[cartItemIndex].quantity === 1) {
          handleRemoveFromCart();
        } else {
          updateCart(name, operation);
        }
        break;
      case 'set':
        if (cart[cartItemIndex].stock >= value) {
          updateCart(name, operation, value);
        } else {
          setNotification({
            message: 'Not Enough in stock',
            severity: 'error',
          });
        }
        break;
      default:
        break;
    }
  };
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className="buttons_group">
        <IconButton onClick={() => editRobotCart('+')}>
          <AddCircleOutline />
        </IconButton>
        <TextField
          variant="outlined"
          value={quantity}
          onChange={(e) => {
            editRobotCart('set', e.target.value);
          }}
        />
        <IconButton onClick={() => editRobotCart('-')}>
          <RemoveCircleOutline />
        </IconButton>
      </div>
    </div>
  );
};
CartQuantityButtons.propTypes = {
  quantity: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  cart: PropTypes.array.isRequired,
  updateCart: PropTypes.func.isRequired,
  setNotification: PropTypes.func.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
};
const mapStateToProps = ({ cart }) => {
  return cart;
};
export default connect(mapStateToProps, {
  updateCart,
  setNotification,
})(CartQuantityButtons);
