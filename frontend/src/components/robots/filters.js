import React from 'react';
import uniqid from 'uniqid';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import { setFilters } from '../../redux/actions/filters.action';

const useStyles = makeStyles((theme) => ({
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

const Filters = ({ materials, setFilters, material }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleChange = (event) => {
    setFilters({ material: event.target.value });
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={material}
          onChange={handleChange}
        >
          {['All', ...materials].map((material) => (
            <MenuItem key={uniqid()} value={material}>
              {material}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

Filters.propTypes = {
  materials: PropTypes.array.isRequired,
  material: PropTypes.string.isRequired,
  setFilters: PropTypes.func.isRequired,
};
const mapStateToProps = ({ filters }) => {
  return filters;
};
export default connect(mapStateToProps, { setFilters })(Filters);
