import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import { connect } from 'react-redux';
import { setFilters } from '../redux/actions/filters.action';
import { Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const SimplePagination = ({
  pageCount,
  setFilters,
  currentPage,
  botsCount,
}) => {
  useEffect(() => {
    if (currentPage > botsCount / 10) {
      setFilters({ currentPage: Math.ceil(botsCount / 10) });
    }
  }, [currentPage, botsCount]);

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography>Page: {currentPage}</Typography>
      <Pagination
        page={currentPage}
        count={pageCount}
        onChange={(event, value) => {
          setFilters({
            currentPage: value,
          });
        }}
      />
    </div>
  );
};
const mapStateToProps = ({ filters }) => filters;
SimplePagination.propTypes = {
  pageCount: PropTypes.number.isRequired,
  setFilters: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
  botsCount: PropTypes.number,
};
export default connect(mapStateToProps, { setFilters })(SimplePagination);
